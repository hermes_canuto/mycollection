package database

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/hermescanuto/mycollection/models"
	"github.com/jinzhu/gorm"
)

func GetGormConn() *gorm.DB {
	db, err := gorm.Open("mysql", "root:1234@tcp(127.0.0.1:3306)/mycollection?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}

	return db
}

func InitialMigration() {
	db := GetGormConn()
	defer db.Close()
	db.Debug().Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&models.CollectionType{}, &models.Collection{}, &models.User{})
	db.Debug().Model(&models.Collection{}).AddForeignKey("collection_type_id", "collection_types(id)", "CASCADE", "CASCADE")

}
