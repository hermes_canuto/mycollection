package router

import (
	"github.com/foolin/echo-template"
	"github.com/hermescanuto/mycollection/api"
	"github.com/hermescanuto/mycollection/controllers"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"html/template"
	"net/http"
	"time"
)

func Index(c echo.Context) error {
	return c.Render(http.StatusOK, "index", echo.Map{"title": "Page file title!!"})
}

func Router(app *echo.Echo) {

	//app.Renderer = echoview.Default()

	//Set a folder for html templates
	app.Renderer = echotemplate.New(echotemplate.TemplateConfig{
		Root:      "web/views",
		Extension: ".html",
		Master:    "layouts/master",
		Partials:  []string{},
		Funcs: template.FuncMap{
			"copy": func() string {
				return time.Now().Format("2006")
			},
		},
		DisableCache: true,
	})

	// Set static folder
	app.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Root:   "web/static",
		Browse: false,
	}))

	app.GET("/", Index)
	app.GET("/list", controllers.List)
	app.POST("/login", controllers.Login)

	//Restrite area
	r := app.Group("/api/")
	r.Use(middleware.JWT([]byte("secret")))
	r.GET("type", api.GetAllTypes)
	r.GET("item/:id", api.GetItem)
	r.GET("item", api.GetAllItem)
	r.POST("item", api.SaveItem)

}
