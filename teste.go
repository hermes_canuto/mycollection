package main

import (
	"github.com/hermescanuto/mycollection/database"
	"github.com/hermescanuto/mycollection/models"
)

func main() {

	database.InitialMigration()
	db := database.GetGormConn()
	defer db.Close()

	//var tipo = models.CollectionType{}
	//tipo.Name = "Vinil"
	//db.Debug().Save(&tipo)
	//des := "The Dark Knight Returns (alternatively titled Batman: The Dark Knight Returns) is a 1986 four-issue comic book miniseries starring Batman, written by Frank Miller, illustrated by Miller and Klaus Janson, and published by DC Comics. When the series was collected into a single volume later that year, the story title for the first issue was applied to the entire series. The Dark Knight Returns tells an alternative story of Bruce Wayne, who at 55 years old returns from retirement to fight crime and faces opposition from the Gotham City police force and the United States government. The story introduces Carrie Kelley as the new Robin and the hyper-violent street gang known as the Mutants. The story also features the return of classic foes such as Two-Face and The Joker, and culminates with a confrontation against Superman, who works on behalf of the government."
	//var co = models.Collection{Name: "The Dark Knight Returns", Year: 1972, Description: des, CollectionTypeID: 1}
	//ok := db.Debug().Save(&co)
	//fmt.Println(ok.Error)

	var co = models.Collection{}
	var tipo = models.CollectionType{}
	db.Debug().First(&co, 2).Related(&tipo)

}
