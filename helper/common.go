package helper

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/hermescanuto/mycollection/database"
	"github.com/hermescanuto/mycollection/models"
	"github.com/labstack/echo"
)

func Whois(c echo.Context) *models.User {
	db := database.GetGormConn()
	defer db.Close()

	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	//name := claims["name"].(string)
	id := claims["id"]
	u := new(models.User)
	db.Where("ID =?", id).Find(&u)
	return u
}
