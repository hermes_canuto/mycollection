package controllers

import (
	_ "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"net/http"
)

func Home(c echo.Context) error {
	return c.HTML(http.StatusOK, "Hello World")
}
