package controllers

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/hermescanuto/mycollection/database"
	"github.com/hermescanuto/mycollection/models"
	"github.com/labstack/echo"
	"net/http"
	"time"
)

func createtoken(u models.User) string {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = u.ID
	claims["name"] = u.Name
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
	t, _ := token.SignedString([]byte("secret"))
	return t

}

func Login(c echo.Context) error {
	u := new(models.User)
	if err := c.Bind(u); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"Error": "Missing fields"})
	}
	user := models.User{}
	db := database.GetGormConn()
	defer db.Close()

	if db.Where("name = ? and password = ?", u.Name, u.Password).Find(&user).RecordNotFound() {
		return c.JSON(http.StatusNotFound, map[string]string{"Error": "not found"})
	}

	fmt.Println(user.Name, user.Password, user.ID)
	t := createtoken(user)
	return c.JSON(http.StatusOK, map[string]string{"token": t})
}

func List(c echo.Context) error {
	return c.Render(http.StatusOK, "list-types", echo.Map{"title": "List of Types"})
}
