package models

import (
	"github.com/jinzhu/gorm"
)

type CollectionType struct {
	gorm.Model
	Name string `db:"name" json:"name"`
}

type Collection struct {
	gorm.Model
	Name             string         `db:"size(150)" json:"name"`
	Year             int            `json:"year"`
	Description      string         `gorm:"type:text"  json:"description"`
	Cover            string         `gorm:"size:400"  json:"cover"`
	CollectionTypeID uint           `sql:"index" json:"collectiontype_id"`
	CollectionType   CollectionType `json:"-"`
}

var CollectionTypes []CollectionType
var Collections []Collection

type User struct {
	gorm.Model
	Name     string `gorm:"type:varchar(100)" json:"name"`
	Password string `gorm:"type:varchar(100)" json:"password"`
}
