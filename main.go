package main

import (
	"github.com/hermescanuto/mycollection/database"
	"github.com/hermescanuto/mycollection/router"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"os"
)

func main() {
	//run the migrations
	database.InitialMigration()

	e := echo.New()

	//define the routes
	router.Router(e)

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
	}))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE, echo.PUT, echo.HEAD, echo.PATCH},
	}))

	//APP_PORT , the port for application to run. "export APP_PORT=1234"
	env := ":" + os.Getenv("APP_PORT")
	e.Logger.Fatal(e.Start(env))
}
