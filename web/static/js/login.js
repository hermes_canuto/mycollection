console.log("Teste geral")
var x = document.cookie;
console.log(getCookie('token'))

$(document).ready(function () {
    console.log("ready!");

    $('#bt').click(function () {
        $('#myModal').modal('show')

        dados = {'name': $('#login').val(), 'password': $('#password').val()}
        $.ajax({
            type: "POST",
            url: "http://localhost:1234/login",
            data: JSON.stringify(dados),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $('#myModal').modal('hide')
                $('.spinner-border').addClass("invisible")
                $('#bt').show()
                $('.modal-body').html("<div>"+data.token+"</div>")
                document.cookie = "token="+data.token;
                window.location.href="/list"
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $('.modal-body').html("<div>"+errorThrown+"</div>")
                console.log(errorThrown);
            }

        });

    });


});
