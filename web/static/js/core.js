function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


function tableinsert(data) {
    var trHTML = '';
    $.each(data, function (i, item) {
        trHTML += `<tr><td>${item.ID}</td><td>${item.name}</td></tr>`
    });
    $('#thetable tbody').html(trHTML);
    $('#spinner').addClass('invisible')
}

function getlisttypes() {
    $.ajax({
        url: 'http://localhost:1234/api/type',
        headers: {
            'Authorization': `Bearer ${token}`,
        },
        method: 'GET',
        success: function (data) {
            console.log('succes: ' + data);
            console.log(data)
            tableinsert(data)
        }
    }, "json");
}