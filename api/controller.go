package api

import (
	"fmt"
	"github.com/hermescanuto/mycollection/database"
	"github.com/hermescanuto/mycollection/helper"
	"github.com/hermescanuto/mycollection/models"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

func SaveItem(c echo.Context) error {
	item := new(models.Collection)
	if err := c.Bind(item); err == nil {
		fmt.Println(err.Error())
		panic("Error")
	}
	db := database.GetGormConn()
	defer db.Close()
	db.Debug().Save(&item)
	rt := map[string]interface{}{"ID": item.ID}
	return c.JSON(http.StatusOK, rt)
}

// Retorna um item
func GetItem(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	db := database.GetGormConn()
	defer db.Close()
	var co = &models.Collection{}
	var tipo = &models.CollectionType{}
	if db.First(co, id).Related(tipo).RecordNotFound() {
		rt := map[string]interface{}{"Error": "not Found"}
		return c.JSON(http.StatusNotFound, rt)
	} else {
		//rt := serializer.Colletion(co, tipo)
		return c.JSON(http.StatusOK, co)
	}
}

// Retorna todos os itens do banco
func GetAllItem(c echo.Context) error {
	db := database.GetGormConn()
	defer db.Close()
	var cos = models.Collections
	if db.Find(&cos).RecordNotFound() {
		rt := map[string]interface{}{"Error": "not Found"}
		return c.JSON(http.StatusNotFound, rt)
	} else {
		return c.JSON(http.StatusOK, cos)
	}

}

// Retorna todos os types do banco
func GetAllTypes(c echo.Context) error {

	u := helper.Whois(c)
	fmt.Println(u.Name, u.ID)
	db := database.GetGormConn()
	defer db.Close()
	var ct = models.CollectionTypes
	if db.Find(&ct).RecordNotFound() {
		rt := map[string]interface{}{"Error": "not Found"}
		return c.JSON(http.StatusNotFound, rt)
	} else {
		return c.JSON(http.StatusOK, ct)
	}
}
